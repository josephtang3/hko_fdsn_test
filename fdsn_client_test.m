# modified from Steven Gibbons
import obspy
from obspy.clients.fdsn import Client
client = Client("IRIS")
# from obspy.clients.fdsn.header import URL_MAPPINGS
# for key in sorted(URL_MAPPINGS.keys()):
#    print("{0:<11} {1}".format(key, URL_MAPPINGS[key]))
from obspy import UTCDateTime
from datetime import datetime
 
date_string = str(input('Enter earthquake date and time (yyyy-mm-dd hh:mm): '))
quake_date = datetime.strptime(date_string, "%Y-%m-%d %H:%M")
# print("quake_date string is: ", quake_date)

network_string = str(input('Enter FDSN network name: '))
station_string = str(input('Enter station name: '))


before_string = str(input('Enter how many minutes to show before quake: '))
# convert minutes to seconds
quake_before = int(before_string) * 60

after_string = str(input('Enter how many minutes to show after quake: '))
# print("hours string: ", after_string)
# convert minutes to seconds
quake_after = int(after_string) * 60
# print(quake_after)

t = UTCDateTime(quake_date)
[ sbef, saft ] = [ quake_before, quake_after ]
st = client.get_waveforms( network_string, station_string, "*", "B??", t - sbef, t + saft, attach_response=True)
st.plot(linewidth=0.76, equal_scale = False)
